<?php
/*
Plugin Name: Eastern Standard WordPress Taxonomy Framework
Plugin URI: https://bitbucket.org/estrnstd/eastern-standard-wordpress-taxonomy-framework
Description: A system for configuring custom taxonomies.
Version: 1.0.0
Author: Eastern Standard
Author URI: https://easternstandard.com
Update URI: eswp-taxonomy-framework
*/

function eswp_taxonomy_framework_initialize($taxonomies_directory) {
	if (substr($taxonomies_directory, -1, 1) !== '/') {
		$taxonomies_directory .= '/';
	}
	$taxonomy_directories = $taxonomies_directory . '*/';
	$taxonomy_directories = glob($taxonomy_directories);
	unset($eswp_taxonomy_data);
	foreach ($taxonomy_directories as $taxonomy_directory) {
		$taxonomy_directory_config_file_path = $taxonomy_directory . 'config.php';
		include $taxonomy_directory_config_file_path;
		if (isset($eswp_taxonomy_data) && is_array($eswp_taxonomy_data)) {
			register_taxonomy($eswp_taxonomy_data['key'], $eswp_taxonomy_data['object_type'], $eswp_taxonomy_data['arguments']);
		}
		else {
			trigger_error(__("Variable 'eswp_taxonomy_data' in {$taxonomy_directory_config_file_path} must be an array."), E_USER_WARNING);
		}
		unset($eswp_taxonomy_data);
	}
}
