# Eastern Standard WordPress Taxonomy Framework
A WordPress plugin to standardize working with taxonomies.

## Installation
Copy the contents of this repository into the plugins directory. The path should look like this: `/wp-content/plugins/eswp-taxonomy-framework/eswp-taxonomy-framework.php`. When you copy this repository into the plugin directory, omit the `.git` folder to prevent issues that arise with nested git repositories.

Activate the plugin via the WordPress dashboard 'Plugins' page (`/wp-admin/plugins.php`).

## Usage

Using the `init` hook, call the `eswp_taxonomy_framework_initialize` function passing in the directory containing the custom taxonomy definitions.
```php
add_action('init', 'eswp_register_custom_taxonomies');
function eswp_register_custom_taxonomies() {
	if (function_exists('eswp_taxonomy_framework_initialize')) {
		eswp_taxonomy_framework_initialize(get_template_directory() . '/custom-taxonomies');
	}
	else {
		trigger_error(__("Function 'eswp_taxonomy_framework_initialize' does not exist. Make sure the 'Eastern Standard WordPress Taxonomy Framework' plugin is installed and activated."), E_USER_WARNING);
	}
}
```

### Creating Taxonomies

Included in this repository is an example taxonomy. You can find it located at the `'example-taxonomy'` directory. It's a good reference for creating a custom taxonomy, but keep in mind: for existing projects, it is **strongly recommended** that you follow any conventions present in the project you're working in. It might be easier to copy an existing custom taxonomy rather than start from this example.

The only required file is a `config.php` file located in the root of the taxonomy directory. This file is what informs the plugin of all the taxonomy configuration data. This file must contain an array named `$eswp_taxonomy_data`.

The values `$eswp_taxonomy_data['key']`, `$eswp_taxonomy_data['object_type']`, and `$eswp_taxonomy_data['arguments']` will be passed into the `register_taxonomy` function in that order. You can read more about the `register_taxonomy` function here: <a href="https://developer.wordpress.org/reference/functions/register_taxonomy" target="_blank">https://developer.wordpress.org/reference/functions/register_taxonomy</a>.